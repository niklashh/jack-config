#[allow(dead_code)]
pub fn to_db(value: f32) -> f32 {
    20.0 * value.log10()
}

pub fn to_linear(value: f32) -> f32 {
    10f32.powf(value / 20.0)
}

pub fn get_sinks(client: &jack::Client) -> Vec<String> {
    let jack_sinks = client.ports(
        Some("PulseAudio JACK Sink"),
        None,
        jack::PortFlags::IS_OUTPUT,
    );
    assert_eq!(jack_sinks.len(), 2);

    jack_sinks
}

pub fn get_sources(client: &jack::Client) -> Vec<String> {
    let jack_sinks = client.ports(
        Some("PulseAudio JACK Source"),
        None,
        jack::PortFlags::IS_INPUT,
    );
    assert_eq!(jack_sinks.len(), 2);

    jack_sinks
}

pub fn get_headphones(client: &jack::Client) -> Vec<String> {
    let headphones = client.ports(Some("jackalsa"), None, jack::PortFlags::IS_INPUT);
    //    assert_eq!(headphones.len(), 2);

    headphones
}

pub fn get_microphone(client: &jack::Client) -> Vec<String> {
    let microphone = client.ports(Some("Headphones in"), None, jack::PortFlags::IS_OUTPUT);
    assert_eq!(microphone.len(), 1);

    microphone
}

pub fn connect_ports(client: &jack::Client, input: &str, output: &str) -> Result<(), jack::Error> {
    match client.connect_ports_by_name(input, output) {
        Err(jack::Error::PortAlreadyConnected(..)) => Ok(()),
        rest => rest,
    }
}

pub fn disconnect_ports(
    client: &jack::Client,
    input: &str,
    output: &str,
) -> Result<(), jack::Error> {
    match client.disconnect_ports_by_name(input, output) {
        Err(jack::Error::PortAlreadyConnected(..)) => Ok(()),
        rest => rest,
    }
}

pub fn game_on() {
    let (client, _status) = jack::Client::new("", jack::ClientOptions::NO_START_SERVER).unwrap();

    let jack_sinks = get_sinks(&client);
    let headphones = get_headphones(&client);

    (0..2).for_each(|i| connect_ports(&client, &jack_sinks[i], &headphones[i]).unwrap());
}

pub fn game_off() {
    let (client, _status) = jack::Client::new("", jack::ClientOptions::NO_START_SERVER).unwrap();

    let jack_sinks = get_sinks(&client);
    let headphones = get_headphones(&client);

    (0..2).for_each(|i| disconnect_ports(&client, &jack_sinks[i], &headphones[i]).unwrap());
}

pub fn connect_microphone() {
    let (client, _status) = jack::Client::new("", jack::ClientOptions::NO_START_SERVER).unwrap();

    let jack_sources = get_sources(&client);
    let microphone = get_microphone(&client);

    let _ = client.connect_ports_by_name(&microphone[0], &jack_sources[0]);
    let _ = client.connect_ports_by_name(&microphone[0], &jack_sources[1]);
}
