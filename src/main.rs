use jack_config::{compressor, utils};

fn main() {
    let args = std::env::args().collect::<Vec<String>>();
    let x = args.iter();

    match x.skip(1).map(AsRef::as_ref).collect::<Vec<_>>().as_slice() {
        &["compressor"] => compressor::run(),
        &["game", "on"] => utils::game_on(),
        &["game", "off"] => utils::game_off(),
        &["connect-microphone"] => utils::connect_microphone(),
        _ => eprintln!("Help usage: TODO"),
    }
}
