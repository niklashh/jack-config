use crate::utils::to_linear;
use ringbuffer::{AllocRingBuffer, RingBuffer};
use std::sync::mpsc;

pub struct Compressor<T>
where
    T: RingBuffer<(f32, f32)>,
{
    desired_loudness: f32,
    /// measured in dB/s
    attack: f32,
    /// measured in dB/s
    release: f32,
    makeup: f32,
    reduction: f32,
    samples: T,
    resolution: i64, // TODO timer
    counter: i64,
    sample_rate: usize,
}

impl<T> Compressor<T>
where
    T: RingBuffer<(f32, f32)>,
{
    /// Reduction is adjusted `resolution` times per
    /// `sample_rate` (from jack)
    pub fn adjust_reduction(&mut self, value: f32) {
        let delta = value / to_linear(self.desired_loudness).powi(2);
        if delta > to_linear(self.reduction).powi(2) {
            self.reduction += self.attack / self.sample_rate as f32 * self.resolution as f32;
        } else {
            self.reduction = f32::max(
                0.0,
                self.reduction - self.release / self.sample_rate as f32 * self.resolution as f32,
            );
        }
    }

    pub fn tick(&mut self, sample: (f32, f32)) {
        self.samples.push(sample);

        if self.counter == 0 {
            let folder = |acc, (l, r): &(f32, f32)| {
                acc + (l.powi(2) * r.powi(2)).sqrt() / self.samples.len() as f32
            };
            // let folder = |acc, &(l, _): &(f32, f32)| f32::max(acc, l);

            let volume_squared = self.samples.iter().fold(0.0, folder);
            self.adjust_reduction(volume_squared);
        }
        self.counter += 1;
        self.counter %= self.resolution;
    }

    // TODO &[(f32, f32)]
    pub fn compress(&mut self, in_slices: (&[f32], &[f32]), out_slices: (&mut [f32], &mut [f32])) {
        for i in 0..in_slices.0.len() as usize {
            let sample = (in_slices.0[i], in_slices.1[i]);
            self.tick(sample);

            out_slices.0[i] = sample.0 / to_linear(self.reduction) * to_linear(self.makeup);
            out_slices.1[i] = sample.1 / to_linear(self.reduction) * to_linear(self.makeup);
        }
    }
}

pub fn run() {
    let (client, status) =
        jack::Client::new("jack_config_client", jack::ClientOptions::NO_START_SERVER).unwrap();

    assert!(status.is_empty());

    let in_l = client
        .register_port("in_l", jack::AudioIn::default())
        .unwrap();
    let in_r = client
        .register_port("in_r", jack::AudioIn::default())
        .unwrap();
    let mut out_l = client
        .register_port("out_l", jack::AudioOut::default())
        .unwrap();
    let mut out_r = client
        .register_port("out_r", jack::AudioOut::default())
        .unwrap();

    let name_in_l = in_l.name().unwrap();
    let name_in_r = in_r.name().unwrap();
    let name_out_l = out_l.name().unwrap();
    let name_out_r = out_r.name().unwrap();

    let mut compressor = Compressor {
        desired_loudness: -30.0,
        attack: 100.0,
        release: 3.0,
        makeup: 0.0,
        reduction: 0.0,
        samples: AllocRingBuffer::with_capacity(32),
        resolution: 16,
        counter: 0,
        sample_rate: client.sample_rate(),
    };

    let process_callback = move |_: &jack::Client, ps: &jack::ProcessScope| -> jack::Control {
        let in_l_slice = in_l.as_slice(ps);
        let in_r_slice = in_r.as_slice(ps);

        let out_l_slice = out_l.as_mut_slice(ps);
        let out_r_slice = out_r.as_mut_slice(ps);

        compressor.compress((in_l_slice, in_r_slice), (out_l_slice, out_r_slice));

        // println!(
        //     "Reduction: {:4.1}dB {}",
        //     compressor.reduction,
        //     std::iter::repeat(".")
        //         .take((compressor.reduction * 2.0) as usize)
        //         .collect::<String>()
        // );

        jack::Control::Continue
    };
    let process = jack::ClosureProcessHandler::new(process_callback);

    // Activate the client, which starts the processing.
    let client = client.activate_async((), process).unwrap();

    let (tx, rx) = mpsc::channel();
    ctrlc::set_handler(move || {
        eprintln!(" Quitting...");
        tx.send(()).unwrap();
    })
    .expect("Error setting Ctrl-C handler");

    let jack_sinks = crate::utils::get_sinks(client.as_client());
    let headphones = crate::utils::get_headphones(client.as_client());

    // Must connect after activating
    let _ = client
        .as_client()
        .connect_ports_by_name(&jack_sinks[0], &name_in_l);
    //         .expect("able to connect port");
    let _ = client
        .as_client()
        .connect_ports_by_name(&jack_sinks[1], &name_in_r);
    //        .expect("able to connect port");

    rx.recv().unwrap();

    match client.deactivate() {
        Ok(_) => (),
        Err(err) => eprintln!("Error deactivating client: {:?}. Is jack still alive?", err),
    };
}
